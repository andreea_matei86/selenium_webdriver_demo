import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.junit.Test;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Andreea Pintilie on 2/23/2017.
 */
public class Selenium_Actions {
    private WebDriver driver;

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "./src/resources/chromedriver.exe");
        driver = new ChromeDriver();
    }


    @Test
    public void click() throws InterruptedException {
        driver.get(Sites.PERIODIC_TABLE);
        Thread.sleep(3000);
        WebElement helix = driver.findElement(By.id("helix"));
        Actions seleniumActions = new Actions(driver);
        seleniumActions.click(helix).perform();
    }

    @Test
    public void doubleClick() {
        driver.get(Sites.DOUBLE_CLICK);
        WebElement element = driver.findElement(By.xpath("//button"));
        Actions seleniumActions = new Actions(driver);
        seleniumActions.doubleClick(element).perform();
        Assert.assertTrue("The alert window is present", driver.switchTo().alert().getText().contains("You double clicked me"));
    }


    @Test
    public void contextClick() throws InterruptedException {
        driver.get(Sites.PERIODIC_TABLE);
        Thread.sleep(3000);
        WebElement element = driver.findElement(By.id("helix"));
        Actions seleniumActions = new Actions(driver);
        seleniumActions.contextClick(element).perform();
    }

    @Test
    public void clickAndHoldAndRelease() {
        driver.get(Sites.CLICK_AND_HOLD);
        driver.switchTo().frame(0);
        List<WebElement> allSelectables = driver.findElements(By.cssSelector("ol#selectable *"));
        Actions seleniumActions = new Actions(driver);

        //for each element in the list we perform a click and hold operation
        allSelectables.stream().forEach(element -> seleniumActions.clickAndHold(element));

        //at the end, we release the mouse action
        seleniumActions.release().perform();
    }

    @Test
    public void moveToElement() throws InterruptedException {
        driver.get(Sites.MOVE_TO_ELEMENT);
        WebElement menuItem = driver.findElement(By.cssSelector("[class^=\"expanded menu-mlid-5821\"]"));
        Actions seleniumActions = new Actions(driver);
        seleniumActions.moveToElement(menuItem).perform();
        WebElement dropDownMenuItem = driver.findElement(By.cssSelector("[class^=\"first expanded menu-mlid-5846\"]>span"));
        Assert.assertTrue(dropDownMenuItem.isDisplayed());
    }

    @Test
    public void moveByOffset() throws InterruptedException {
        driver.get(Sites.MOVE_TO_ELEMENT);
        driver.manage().window().maximize();
        Actions seleniumActions = new Actions(driver);
        seleniumActions.moveByOffset(450, 75).perform();
        WebElement dropDownMenuItem = driver.findElement(By.cssSelector("[class^=\"first expanded menu-mlid-5846\"]>span"));
        Assert.assertTrue(dropDownMenuItem.isDisplayed());
    }


    @Test
    public void dragAndDrop() {
        driver.get(Sites.DRAG_AND_DROP);
        driver.switchTo().frame(0);
        WebElement drag = driver.findElement(By.id("draggable"));
        WebElement drop = driver.findElement(By.id("droppable"));
        Actions seleniumActions = new Actions(driver);
        seleniumActions.dragAndDrop(drag, drop).perform();
        Assert.assertTrue("Element was dropped", drop.getText().contains("Dropped"));
    }

    @Test
    public void dragAndDropBy() {
        driver.get(Sites.DRAG_AND_DROP_BY);
        driver.switchTo().frame(0);
        WebElement drag = driver.findElement(By.id("draggable"));
        Actions seleniumActions = new Actions(driver);
        IntStream.range(0,8).forEach(index->seleniumActions.dragAndDropBy(drag, 10*index, 10*index).perform());
    }

    @Test
    public void keys_up_down_send_keys() throws InterruptedException {
        driver.get(Sites.CLOUD_GENERATOR);

        // generating a string composed of universally unique identifiers for the word cloud
        List list = Stream.generate(()->UUID.randomUUID().toString().replace("-", " ")).limit(100).collect(Collectors.toList());
        String generated = list.stream().collect(Collectors.joining(StringUtils.SPACE)).toString();

        WebElement textArea = driver.findElement(By.id("text"));
        WebElement goButton = driver.findElement(By.id("go"));
        Actions seleniumActions = new Actions(driver);

        //performing Ctrl+A -> Delete -> type the generated string
        seleniumActions.keyDown(textArea, Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(Keys.DELETE).sendKeys(generated).perform();

        driver.findElement(By.id("go")).click();
    }

    @After
    public void exitDriver() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }

}
