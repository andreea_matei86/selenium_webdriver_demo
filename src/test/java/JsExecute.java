import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class JsExecute {
    private ChromeDriver driver;
    private JavascriptExecutor javascriptExecutor;

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "./src/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }

    @Test
    public void testExecuteAsync() {
        // when we execute an async script we will get control after the arguments[arguments.length - 1] method is called.
        long start = System.currentTimeMillis();
        driver.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 500);");
        long elapsed = System.currentTimeMillis() - start;
        System.out.println("Elapsed time: " + elapsed);
        Assert.assertTrue("AsyncFunction is waiting for callback",elapsed >= 500);
    }
    @Test
    public void testExecuteSync() {
        // when we execute an synchronous script the control will return immediately.
        long start = System.currentTimeMillis();
        driver.executeScript("window.setTimeout(arguments[arguments.length - 1], 500);");
        long elapsed = System.currentTimeMillis() - start;
        System.out.println("Elapsed time: " + elapsed);
        Assert.assertTrue("sync is NOT waiting for callback",elapsed < 500);
    }

    @Test
    public void testClickWithJs() throws InterruptedException {
        // we can use js to interact or read different information from dom
        driver.get(Sites.SITE_WITH_IFRAMES);
        WebElement element = driver.findElement(By.xpath("//a[text()='Try it Yourself »']"));
        new WebDriverWait(driver,3).until(ExpectedConditions.visibilityOf(element));
        // we read the class for the element (should be w3-btn w3-margin-bottom)
        String elementClassbyJS = (String) driver.executeScript("return arguments[0].className",element);
        Assert.assertEquals("Class should be as expected",elementClassbyJS,"w3-btn w3-margin-bottom");
        // let's try the classic webdriver way:
        String  elementClassByWebdriver = element.getAttribute("class");
        Assert.assertEquals("Class should be as expected",elementClassbyJS, elementClassByWebdriver);

        // now we click the button the element using javascript
        driver.executeScript("arguments[0].click()", element);
        // 2 tabs should be opened
        Assert.assertEquals("js Click should open second tab",2, driver.getWindowHandles().size());

    }

    @Test
    public void testScrollInView() throws InterruptedException {
        // we can use execute javascript to bring an element in viewport
        driver.get(Sites.SITE_WITH_IFRAMES);
        WebElement element = driver.findElement(By.xpath("//a[text()='Try it Yourself »']"));
        //wait for the element to apear on the page
        new WebDriverWait(driver,3).until(ExpectedConditions.visibilityOf(element));
        driver.executeScript("arguments[0].scrollIntoView(false)",element);
        // highlight the element in the page
        Utils.highlight(driver, element);
    }

    @Test
    public void testExecuteScript() {
        openPage();
        long start = System.currentTimeMillis();
        Object a = driver.executeScript("return document.getElementById('txt').innerHTML");
        System.out.println(a);
        System.out.println(
                "Elapsed time: " + (System.currentTimeMillis() - start));
    }

    @Test
    public void testJSExecutorAsynchronous() {
        openPage();
        long start = System.currentTimeMillis();
        Object a = driver.executeAsyncScript(
                "arguments[arguments.length - 1](document.getElementById('txt').innerHTML)");
        System.out.println(a);
        System.out.println(
                "Elapsed time: " + (System.currentTimeMillis() - start));
    }

    public void openPage() {
        // opens an invalid page
        driver.get(Sites.SITE_FOR_ALERT);
        //sets a timer inside the window
        driver.executeScript("document.write(\"<div id='txt'>This is 'myWindow'</div>\");");
        driver.executeScript("function startTime(){var today=new Date();var h=today.getHours();var m=today.getMinutes();" +
                "var s=today.getSeconds();m=checkTime(m);s=checkTime(s);" +
                "document.getElementById(\"txt\").innerHTML = h+ \":\" + m + \":\" + s;t=setTimeout(function(){startTime()},500);}\n" +
                "function checkTime(i){if(i<10){i=\"0\"+i;}\n" +
                "return i;};startTime();");

    }

}
