import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by dambroze on 5/2/2017.
 */
public class Utils {
    public static void highlight(WebDriver driver, WebElement element) throws InterruptedException {
        Object initialStyle = ((JavascriptExecutor) driver).executeScript("return arguments[0].style", element);
        for (int i = 0; i < 5; i++) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.border = '4px solid red'", element);
            Thread.sleep(200);
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.border = '4px solid yellow'", element);
            Thread.sleep(200);
        }
        ((JavascriptExecutor) driver).executeScript("arguments[0].style = 'arguments[1]'", element, initialStyle);
    }
}
