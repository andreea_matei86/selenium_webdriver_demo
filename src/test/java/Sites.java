/**
 * Created by dambroze on 2/24/2017.
 */
public class Sites {
    public static final String SITE_POPUP_AUTH = "https://www.engprod-charter.net/";
    public static final String SITE_WITH_IFRAMES = "https://www.w3schools.com/html/html_iframe.asp";
    public static final String SITE_FOR_WINDOW_SWITCH = "http://www.endava.com";
    public static final String CLOUD_GENERATOR = "https://www.jasondavies.com/wordcloud/";
    public static final String PERIODIC_TABLE = "https://threejs.org/examples/css3d_periodictable.html";
    public static final String DOUBLE_CLICK = "http://only-testing-blog.blogspot.ro/2014/09/selectable.html";
    public static final String CLICK_AND_HOLD = "http://jqueryui.com/selectable/";
    public static final String DRAG_AND_DROP = "http://jqueryui.com/droppable/";
    public static final String DRAG_AND_DROP_BY = "http://jqueryui.com/draggable/";
    public static final String MOVE_TO_ELEMENT ="https://www.whitehouse.gov/";
    public static final String SITE_FOR_ALERT = "http://localhost" ;
}
