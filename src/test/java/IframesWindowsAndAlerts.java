import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;

/**
 * Created by dambroze on 2/24/2017.
 */
public class IframesWindowsAndAlerts {
    private ChromeDriver driver;

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "./src/resources/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @After
    public void tearDown() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }

    @Test
    public void testWindows() throws InterruptedException {

        //Open main page
        driver.get(Sites.SITE_FOR_WINDOW_SWITCH);

        //check we have only one opened tab
        Assert.assertEquals("We don't have only one opened tab", 1, driver.getWindowHandles().size());
        Assert.assertEquals("Focus is not set on Endava tab", "Endava", driver.getTitle());

        //Open another tab click on "Learn more"
        WebElement learnMore = driver.findElement(By.xpath("//a[text()='Learn More']"));
        new Actions(driver).moveToElement(learnMore).keyDown(Keys.CONTROL).click().keyUp(Keys.CONTROL).build().perform();

        //Check another tab is opened
        Assert.assertEquals("We don't have only 2 opened tab", 2, driver.getWindowHandles().size());

        //Switch to the second tab
        ArrayList<String> tabs2 = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));

        //Verify the tab is switched
        Assert.assertEquals("Focus is not set on Endava tab", "Privacy Policy", driver.getTitle());
        WebElement title = driver.findElement(By.xpath("//h2"));
        Assert.assertEquals("Title is not correct", "Privacy Policy".toUpperCase()
                , title.getText().toUpperCase());

        //Close the last tab
        driver.close();

        //Check last tab is closed
        Assert.assertEquals("We don't have only 1 opened tab", 1, driver.getWindowHandles().size());

        //        Assert.assertEquals("Focus is not set on Endava tab", "Endava", driver.getTitle());

        //Switch back to main window
        driver.switchTo().window(tabs2.get(0));

        //check we have only one opened tab
        Assert.assertEquals("Focus is not set on Endava tab", "Endava", driver.getTitle());

    }

    @Test
    public void testIframes() throws InterruptedException {
        //Open main page
        driver.get(Sites.SITE_WITH_IFRAMES);
        WebElement iframe = driver.findElement(By.xpath("//iframe[@src='default.asp']"));
        Utils.highlight(driver, iframe);
        driver.switchTo().frame(iframe);
        Utils. highlight(driver, driver.findElement(By.xpath("//a[text()='HTML']")));

    }


    @Test
    public void testAlerts() throws InterruptedException {
        driver.get(Sites.SITE_FOR_ALERT);
        driver.executeScript("alert('Pop Up an alert')");
        Thread.sleep(5000);
        Alert alert = driver.switchTo().alert();
        Assert.assertEquals("Pop Up an alert", alert.getText());
        alert.accept();
        Thread.sleep(2000);
    }

}
